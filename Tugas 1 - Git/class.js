//Soal 1 - Animal Class
//Release 0
console.log("Soal 1 - Animal Class")
console.log("Release 0")

class Animal {
    constructor(name){
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }

    get_legs(){
        return this.legs
    }

    set_legs(legs){
        this.legs = legs
    }

    get_name(){
        return this.name
    }

    set_name(name){
        this.name = name
    }

    get_cold_blooded(){
        return this.cold_blooded
    }

    set_cold_blooded(cold_blooded){
        this.cold_blooded = cold_blooded
    }
}

var sheep = new Animal("shaun");

console.log(sheep.get_name()) // "shaun"
console.log(sheep.get_legs()) // 4
console.log(sheep.get_cold_blooded()) // false

console.log("\n\nRelease 1")
// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name){
        super(name)
        this.set_legs(2)
    }

    yell(){
        console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name){
        super(name)
    }

    jump(){
        console.log("hop hop")
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//Soal 2 - Function to Class
console.log("\n\nSoal 2 - Function to Class")

class Clock {
    constructor(template){
        this.template = template
    }

    render(){
        this.date = new Date()

        this.hours = this.date.getHours()
        if( this.hours < 10 ) this.hours = '0' + this.hours

        this.mins = this.date.getMinutes()
        if( this.mins < 10 ) this.mins = '0' + this.mins

        this.secs = this.date.getSeconds()
        if( this.secs < 10 ) this.secs = '0' + this.secs

        this.output = this.template.template.toString()
            .replace('h', this.hours)
            .replace('m', this.mins)
            .replace('s', this.secs)

        console.log(this.output)
    }

    timer = ''

    stop(){
        clearInterval(this.timer)
    }

    start(){
        const _this = this
        this.render()
        this.timer = setInterval(function(){_this.render()}, 1000)
    }
}

var clock = new Clock({template: 'h:m:s'});
console.log(clock.template.template)
clock.start(); 
