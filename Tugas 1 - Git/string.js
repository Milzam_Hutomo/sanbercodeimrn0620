//Soal 1

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log("Soal 1\n");
console.log(`${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`);

console.log(`\n${word.concat(" ", second, " ", third, " ", fourth, " ", fifth, " ", sixth, " ", seventh)}`);

var sentence = "I am going to be React Native Developer"; 
sentence = sentence.split(" ");

//Soal 2
var exampleFirstWord = sentence[0]; 
var secondWord = sentence[1]; 
var thirdWord = sentence[2]; 
var fourthWord = sentence[3];
var fifthWord = sentence[4]; 
var sixthWord = sentence[5];
var seventhWord = sentence[6]; 
var eighthWord = sentence[7]; 

console.log("\n\nSoal 2\n");
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)


//Soal 3
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);

console.log("\n\nSoal 3\n");
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

//Soal 4
var sentence3 = 'wow JavaScript is so cool'; 
sentence3 = sentence3.split(" ");

var exampleFirstWord3 = sentence3[0]; 
var secondWord3 = sentence3[1];
var thirdWord3 = sentence3[2]; 
var fourthWord3 = sentence3[3];
var fifthWord3 = sentence3[4];

var firstWordLength = exampleFirstWord3.length  
// lanjutkan buat variable lagi di bawah ini 
console.log("\n\nSoal 4\n");
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3.length); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3.length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3.length); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3.length); 