//Soal if-else
console.log("Soal IF-ELSE\n")
var name = "Sanber";
var role = "Werewolf";

if(name != null){
    if(role != null){
        if(role === "Penyihir"){
            console.log(`Selamat datang di Dunia Werewolf, ${name}\nHalo ${role} ${name}, kamu dapat melihat siapa yang menjadi werewolf!`)
        }else if(role === "Guard"){
            console.log(`Selamat datang di Dunia Werewolf, ${name}\nHalo ${role} ${name}, kamu akan membantu melindungi temanmu dari serangan werewolf`)
        }else{
            console.log(`Selamat datang di Dunia Werewolf, ${name}\nHalo ${role} ${name}, kamu akan memakan mangsa setiap malam!`)
        }
    }else{
        console.log(`Halo ${name}, Pilih peranmu untuk memulai game!`);
    }
}else{
    console.log("Nama harus diisi!")
}

//Soal switch case
console.log("\n\nSoal SWITCH CASE\n")
var tanggal = 17;
var bulan = 6;
var tahun = 2020;

switch(bulan){
    case 1:{
        console.log(`${tanggal} Januari ${tahun}`)
        break;
    }
    case 2:{
        console.log(`${tanggal} Februari ${tahun}`)
        break;
    }
    case 3:{
        console.log(`${tanggal} Maret ${tahun}`)
        break;
    }
    case 4:{
        console.log(`${tanggal} April ${tahun}`)
        break;
    }
    case 5:{
        console.log(`${tanggal} Mei ${tahun}`)
        break;
    }
    case 6:{
        console.log(`${tanggal} Juni ${tahun}`)
        break;
    }
    case 7:{
        console.log(`${tanggal} Juli ${tahun}`)
        break;
    }
    case 8:{
        console.log(`${tanggal} Agustus ${tahun}`)
        break;
    }
    case 9:{
        console.log(`${tanggal} September ${tahun}`)
        break;
    }
    case 10:{
        console.log(`${tanggal} Oktober ${tahun}`)
        break;
    }
    case 11:{
        console.log(`${tanggal} November ${tahun}`)
        break;
    }
    case 12:{
        console.log(`${tanggal} Desember ${tahun}`)
        break;
    }
    default:{
        console.log(`Input tanggal tidak valid`)
    }
}