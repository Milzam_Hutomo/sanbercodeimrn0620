//Soal 1 - Range
console.log("Soal 1 - Range")

function range(startNum, finishNum){
    if(startNum == null || finishNum == null){
        return -1
    }else{
        let number = []
        if(startNum < finishNum){
            for(startNum; startNum <= finishNum; startNum++){
                number.push(startNum)
            }
        }else{
            for(startNum; startNum >= finishNum; startNum--){
                number.push(startNum)
            }
        }
        return number    
    }
}

console.log(range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())

//Soal 2 - Range with Step
console.log("\n\nSoal 2 - Range with Step")
function rangeWithStep(startNum, finishNum, step){
    if(startNum == null || finishNum == null){
        return -1
    }else{
        let number = []
        if(startNum < finishNum){
            for(startNum; startNum <= finishNum; startNum += step){
                number.push(startNum)
            }
        }else{
            for(startNum; startNum >= finishNum; startNum -= step){
                number.push(startNum)
            }
        }
        return number
    }
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

//Soal 3 - Sum of Range
console.log("\n\nSoal 3 - Sum of Range")

function sum(startNum = 0, finishNum = 0, step = 1){
    if(finishNum == 0){
        return startNum
    }else{
        let sum = 0
        if(startNum < finishNum){
            for(startNum; startNum <= finishNum; startNum += step){
                sum += startNum
            }
        }else{
            for(startNum; startNum >= finishNum; startNum -= step){
                sum += startNum
            }
        }
        return sum
    }
}

console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

//Soal 4 - Array Multidimensi
console.log("\n\nSoal 4 - Array Multidimensi")

function dataHandling(data){
    for(let i = 0; i < data.length; i++){
        console.log(`Nama ID: ${data[i][0]}\nNama Lengkap: ${data[i][1]}\nTTL: ${data[i][2]} ${data[i][3]}\nHobi: ${data[i][4]}\n`)
    }
}

function dataHandlingAlternatif(data){
    for(let i = 0; i < data.length; i++){
        const [id, name, city, date, hobby] = data[i]
        console.log(`Nomor ID: ${id}\nNama Lengkap: ${name}\nTTL: ${city} ${date}\nHobi: ${hobby}\n`)
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input)
console.log("With Destructing Array")
dataHandlingAlternatif(input)

//Soal 5 - Balik Kata
console.log("\n\nSoal 5 - Balik Kata")

function balikKata(word){
    let reverse = ""
    for(let i = word.length - 1; i >= 0; i--){
        reverse += word[i]
    }
    return reverse
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

//Soal 6 - Metode Array
console.log("\n\nSoal 6 - Metode Array")

function dataHandling2(data){
    data.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    data.splice(4, 2, "Pria", "SMA Internasional Metro")
    
    console.log(data)

    let date = data[3]
    date = date.split("/")

    switch(parseInt(date[1])){
        case 1:
            console.log("Januari")
            break
        case 2:
            console.log("Februari")
            break
        case 3:
            console.log("Maret")
            break
        case 4:
            console.log("April")
            break
        case 5:
            console.log("Mei")
            break
        case 6:
            console.log("Juni")
            break
        case 7:
            console.log("Juli")
            break
        case 8:
            console.log("Agustus")
            break
        case 9:
            console.log("September")
            break
        case 10:
            console.log("Oktober")
            break
        case 11:
            console.log("November")
            break
        case 12:
            console.log("Desember")
            break
        default:
            console.log("Input tidak valid")
    }

    function sortDate(value){
        value = value.split("/")
        for(let i = 0; i < value.length; i++){
            for(let j = i+1; j < value.length; j++){
                if(parseInt(value[i]) < parseInt(value[j])){
                    let temp = value[i]
                    value[i] = value[j]
                    value[j] = temp
                }
            }
        }
        console.log(value)
    }
    
    sortDate(data[3])

    console.log(date.join("-"))

    console.log(data[1].slice(0, 15))
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

dataHandling2(input)