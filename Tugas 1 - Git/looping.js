console.log(`No. 1 - Looping While`);

let iter = 2;

console.log(`\nLOOPING PERTAMA`);
while(iter <= 20){
    console.log(`${iter} - I love coding`);
    iter += 2;
}

console.log(`\nLOOPING KEDUA`);
do{
    iter -= 2;
    console.log(`${iter} - I will become a mobile developer`);
}while(iter-2 > 0);

console.log(`\nNo. 2 - Looping menggunakan for`);

for(let i = 1; i <= 20; i++){
    if(i % 2 != 0 && i % 3 == 0){
        console.log(`${i} - I Love Coding`);
    }else if(i % 2 != 0){
        console.log(`${i} - Santai`);
    }else if(i % 2 == 0){
        console.log(`${i} - Berkualitas`)
    }
};

console.log(`\nNo. 3 - Membuat Persegi Panjang`);

for(let i = 0; i < 4; i++ ){
    let output = "";
    for(let j = 0; j < 8; j++ ){
        output += "#";
    }
    console.log(`${output}`);
}

console.log(`\nNo. 4 - Membuat Tangga`);

for(let i = 0; i < 7; i++ ){
    let output = "";
    for(let j = 0; j < i+1; j++ ){
        output += "#";
    }
    console.log(`${output}`);
}

console.log(`\nNo. 5 - Membuat Membuat Papan Catur`);

for(let i = 0; i < 8; i++ ){
    let output = "";
    for(let j = 0; j < 8; j++ ){
        if(i % 2 == 0){
            if(j % 2 != 0){
                output += "#";
            }else{
                output += " ";
            }
        }else{
            if(j % 2 != 0){
                output += " ";
            }else{
                output += "#";
            }
        }
    }
    console.log(`${output}`);
}