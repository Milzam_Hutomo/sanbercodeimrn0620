//Soal 1
console.log("Soal Nomor 1");

function teriak(){
    return "Halo Sanbers!";
}

console.log(teriak());

//Soal 2
console.log("\n\nSoal Nomor 2");

function kalikan(x, y){
    return x * y;
}

var num1 = 12;
var num2 = 4;

var hasilkali = kalikan(num1, num2);
console.log(hasilkali);

//Soal 3
console.log("\n\nSoal Nomor 3");

function introduce(name, age, add, hobby){
    return (`Nama saya ${name}, umur saya ${age}, alamat saya di ${add}, dan saya punya hobby yaitu ${hobby}!`);
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);