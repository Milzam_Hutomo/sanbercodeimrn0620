//Soal 1 - Array to Object
console.log("Soal 1 - Array to Object")

function arrayToObject(arr) {
    // Code di sini 
    if(arr.length){
        let now = new Date()
        const thisYear = now.getFullYear()
    
        let people = {
            firstName: "",
            lastName: "",
            gender: "",
            age: ""
        }

        for(let i = 0; i < arr.length; i++){
            let [firstName, lastName, gender, age] = arr[i]
            people.firstName = firstName
            people.lastName = lastName
            people.gender = gender
            
            if(age != null && age < thisYear){
                people.age = thisYear - age
            }else{
                people.age = "Invalid Birth Year"
            }

            console.log(`${i+1}. ${firstName} ${lastName}:`)
            console.log(people)
        }
    }else{
        console.log("")
    }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

// Error case 
arrayToObject([]) // ""

//Soal 2 - Shopping Time
console.log("\n\nSoal 2 - Shopping Time")

function shoppingTime(memberId, money) {
// you can only write your code here!

    const shopItem = {
        "Sepatu Stacattu": 1500000,
        "Baju Zoro": 500000,
        "Baju H&N": 250000,
        "Sweater Uniklooh": 175000,
        "Casing Handphone": 50000
    }

    let shopData = {
        memberId: memberId,
        money: money, 
        listPurchased: [],
        changeMoney: 0
    }

    if(memberId == undefined){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if(memberId.length == 0){
        return "Mohon maaf, toko X hanya berlaku untuk member sahaja"
    }else if(money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }else{
        let shop = 0
        let cart = []
        for(let i = 0; i < Object.values(shopItem).length; i++){
            if(money < Object.values(shopItem)[i]){
                continue
            }
            shop += Object.values(shopItem)[i]
            cart.push(Object.keys(shopItem)[i])
            money -= Object.values(shopItem)[i]

            if(money < Object.values(shopItem)[i+1]) break
        }
        
        shopData.listPurchased = cart
        shopData.changeMoney = money

        return shopData
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal 3 - Naik Angkot
console.log("\n\nSoal 3 - Naik Angkot")

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    let passenger = []
    if(!arrPenumpang.length){
        return passenger
    }else{

        for(let i = 0; i < arrPenumpang.length; i++){
            const route = {
                penumpang: "",
                naikDari: "",
                tujuan: "",
                bayar: 0
            }

            let [name, start, stop] = arrPenumpang[i]
            route.penumpang = name
            route.naikDari = start
            route.tujuan = stop
            route.bayar = (rute.indexOf(stop) - rute.indexOf(start)) * 2000
            
            passenger.push(route)
            
        }
        return passenger
    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));

console.log(naikAngkot([])); //[]