var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let time = 10000

let index = 0
readBooks(time, books[index], function(usedTime){
    readBooks(usedTime, books[index+1], function(usedTime){
        readBooks(usedTime, books[index+2], function(){
            console.log()
        })
    })
})