var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let time = 10000
readBooksPromise(time, books[0])
    .then(function (usedTime){
        readBooksPromise(usedTime, books[1])
        .then(function (usedTime){
            readBooksPromise(usedTime, books[2])
            .catch()
        })
        .catch()
    })
    .catch()